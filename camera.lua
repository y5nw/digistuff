local detector = minetest.registered_nodes["digistuff:detector"]

minetest.register_node("digistuff:camera", {
	tiles = {
		"digistuff_camera_top.png",
		"digistuff_camera_bottom.png",
		"digistuff_camera_right.png",
		"digistuff_camera_left.png",
		"digistuff_camera_back.png",
		"digistuff_camera_front.png",
	},
	digiline = detector.digiline,
	_digistuff_channelcopier_fieldname = detector._digistuff_channelcopier_fieldname,
	groups = {cracky=2, not_blocking_trains = 1},
	paramtype = "light",
	paramtype2 = "facedir",
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {
				{-0.1,-0.5,-0.28,0.1,-0.3,0.3}, --Camera Body
				{-0.045,-0.42,-0.34,0.045,-0.36,-0.28}, -- Lens
				{-0.05,-0.9,-0.05,0.05,-0.5,0.05}, --Pole
			}
	},
	selection_box = {
		type = "fixed",
		fixed = {
				{-0.1,-0.5,-0.34,0.1,-0.3,0.3}, --Camera Body
			}
	},
	description = "Digilines Camera",
	on_construct = detector.on_construct,
	on_receive_fields = detector.on_receive_fields,
	sounds = default and default.node_sound_stone_defaults()
})

minetest.register_craft({
	output = "digistuff:camera",
	recipe = {
		{"homedecor:plastic_sheeting","homedecor:plastic_sheeting","homedecor:plastic_sheeting"},
		{"default:glass","mesecons_luacontroller:luacontroller0000","digilines:wire_std_00000000"},
		{"homedecor:plastic_sheeting","homedecor:plastic_sheeting","homedecor:plastic_sheeting"},
	}
})
